import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;

/**
 * Main FileOrganiser
 * This is the 'main' controlling class of the back-end
 * @author ashley
 *
 */
public class FileOrganiser implements Runnable {
	private final File dir;
	private List<String> fileNames;
	private List<File> listOfFiles;
	private HashSet<String> fTypes;
	private SortStrategy sortStrat;
	private boolean targetAllFiles;
	
	public FileOrganiser (String targetDir) {
		// Sets the target file to sort
		dir = new File(targetDir);
		
		// Initializes the variables
		this.fileNames = new LinkedList<String>();
		this.listOfFiles = new LinkedList<File>();		
		this.fTypes = new HashSet<String>();
		this.targetAllFiles = true;
		
		for (File temp : dir.listFiles()) {
			if (temp.getName().charAt(0) != '.' && !temp.isDirectory()) {
				System.out.println(temp.getName());
				listOfFiles.add(temp);
			}
		}
	}
	
	/**
	 * Displays all the names of the files which will be sorted
	 */
	public void displayFileNames () {
		for (String file : fileNames) {
			System.out.println(file);
		}
	}
	
	/**
	 * Sort strategy used to organise data
	 * @param s Organisation strategy
	 */
	public void setStrategy(SortStrategy s) {
		sortStrat = s;
	}
	
	/**
	 * Returns a list of the files in the directory
	 * @return The names of the files to be organised
	 */
	public List<String> getFileNames () {
		return fileNames;
	}
	
	/**
	 * Adds a sort parameter
	 * @param s Sort option
	 */
	public void addStrategy (SortStrategy s) {
		sortStrat = s;
	}
	
	public void organise () {
		sortStrat.organise(listOfFiles, dir);
	}

	/**
	 * Determines the files within the directory which will be processed
	 * Whether the user has restricted the application to specific 
	 * extensions is also considered
	 * @param types String of extensions
	 */
	public void setFileTypes (String types) {
		// If string is null then only 
		// files with a null extension will be targeted
		if (types != null) {
			for(String temp: types.split(" ")){
				fTypes.add(temp);
			}
		}
		// Adds files which meet the specific requirements
		// to a list of files to be processed
		for (File temp : listOfFiles) {
			String[] splitFName = temp.getName().split("\\.");
			if (fTypes.size() == 0 && !targetAllFiles) {
				if (splitFName.length != 1)	{
					listOfFiles.remove(temp);
				}
			} else {
				boolean extPresent = false;
				for (String ext : fTypes) {
					System.out.println(ext);
					if (splitFName[splitFName.length - 1].equals(ext)) {
						extPresent = true;
						break;
					}
				}
				if (!extPresent) listOfFiles.remove(temp);
			}
		}
	}
	
	/**
	 * Whether to target all files regardless
	 * of extensions
	 * @param value true or false
	 */
	public void setTargetAllFiles (boolean value) {
		targetAllFiles = value;
	}
	
	@Override
	public void run() {
		organise();
	}
}
