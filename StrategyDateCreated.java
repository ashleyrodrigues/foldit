import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;


public class StrategyDateCreated implements SortStrategy {

	/**
	 * Sort strategy to organise files according to date created
	 * @author ashley
	 *
	 */
	
	@Override
	public void organise(List<File> listOfFiles, File dir) {
		for (File temp : listOfFiles) {
			String createDate = "0000-00-00";
			try {
				BasicFileAttributes attr = Files.readAttributes(temp.toPath(), BasicFileAttributes.class);
				createDate = attr.creationTime().toString().substring(0, 10);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			File dest = new File(dir.getAbsolutePath() + "/" + createDate + "/");
			if (!dest.exists()) {
				dest.mkdirs();
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}			
		}
	}
	
}
