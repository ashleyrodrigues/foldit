import java.io.File;
import java.util.List;


public interface SortSubstrStrategy {
	public void organise(List<File> listOfFiles, File dir, String substr);
}
