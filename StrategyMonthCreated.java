import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.List;


public class StrategyMonthCreated implements SortStrategy {

	@Override
	public void organise(List<File> listOfFiles, File dir) {
		for (File temp : listOfFiles) {
			SimpleDateFormat sdf = new SimpleDateFormat("MM");

			File dest = new File(dir.getAbsolutePath() + "/" + sdf.format(temp.lastModified()) + "/");
			if (!dest.exists()) {
				dest.mkdirs();
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}			
		}
	}
}
