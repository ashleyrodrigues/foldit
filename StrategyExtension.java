import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.List;


public class StrategyExtension implements SortStrategy {

	/**
	 * Sort strategy to organise files according to extension of file
	 * @author ashley
	 *
	 */
	
	@Override
	public void organise(List<File> listOfFiles, File dir) {
		for (File temp : listOfFiles) {
			String[] splitString = temp.getName().split("\\.");
			String ext = "noExt";
			if (splitString.length > 1) {
				ext = splitString[splitString.length-1];
			} 
			
			
			File dest = new File(dir.getAbsolutePath() + "/" +  ext + "/");
			if (!dest.exists()) {
				dest.mkdirs();
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}			
		}
	}
	
}
