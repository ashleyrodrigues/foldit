import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MainWindow {
	private JFrame mainFrame;
	private MainMenuPanel main;
	
	/**
	 * Bootstraps the main window
	 * @param args
	 */
	public static void main(String[] args) {
		final MainWindow mw = new MainWindow();
		
		// display the main window in a different thread.
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	mw.display();
            }
        });
	}
	
	/** 
	 * Creates the main JFrame and the main menu panel
	 */
	public MainWindow () {
		mainFrame = new JFrame("Fold It");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		

		// Sets screen size which is a fixed value for the moment
		mainFrame.setPreferredSize(new Dimension(700, 600));		
		main = new MainMenuPanel();
	}
	
	/**
	 *  Function used to display the main menu panel
	 */
	public void display () {
		mainFrame.getContentPane().add(main);
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
}
