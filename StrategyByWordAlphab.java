import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.text.SimpleDateFormat;

public class StrategyByWordAlphab implements SortSubstrStrategy {

		@Override
		public void organise(List<File> listOfFiles, File dir, String word) {
			for (File temp : listOfFiles) {
				String fName = temp.getName().split("\\.")[0];
				int traverseLength = fName.length() - word.length();
				for (int i = 0; i < traverseLength; i++) {
					String substr = fName.substring(i, i + word.length());
					if (substr.equals(word)) {
						File dest = new File(dir.getAbsolutePath() + "/" + word + "/");
						if (!dest.exists()) {
							dest.mkdirs();
							try {
								Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							try {
								Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}		
			}
		}


}
