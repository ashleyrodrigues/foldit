import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.text.SimpleDateFormat;

/**
 * Sort strategy to organise files according to first character
 * @author ashley
 *
 */
public class StrategyFirstChar implements SortStrategy {
	
	@Override
	public void organise(List<File> listOfFiles, File dir) {
		for (File temp : listOfFiles) {
			char c = temp.getName().charAt(0);
			File dest = new File(dir.getAbsolutePath() + "/" + String.valueOf(c) + "/");
			if (!dest.exists()) {
				dest.mkdirs();
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					Files.move(temp.toPath(), Paths.get(dest.getAbsolutePath() + "/" + temp.getName()), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
	}
	
}
