import java.io.File;
import java.util.List;

/**
 * Specific sort strategy used to sort data
 * @author ashley
 *
 */

public interface SortStrategy {
	public void organise(List<File> listOfFiles, File dir);
}
