import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

import java.util.List;
import java.util.LinkedList;

/**
 * The main menu panel
 * @author ashley
 *
 */

public class MainMenuPanel extends JPanel implements ActionListener, KeyListener {
	
	private MainMenuPanel main;
	private JPanel top;
	private JPanel body;
	private JPanel lMargin;
	private JPanel rMargin;
	private JPanel bottom;
	private JTextField dirText;
	private JTextField substrField;
	private JTextField fileTypesText;
	private JButton browse;
	private JButton helpButton;
	private JButton advFeatures;
	private JButton done;
	private JRadioButton allFiles;
	private boolean isSortSelected;
	private List<JRadioButton> sortOpts;
	private List<JRadioButton> advSortOpts;		
	private List<JRadioButton> uniqueSortOpts;
	
	
	public MainMenuPanel () {
		// Creates margin panels and stores
		// a copy of this instance
		main = this;
		top = new JPanel();
		body = new JPanel();
		lMargin = new JPanel();
		rMargin = new JPanel();
		bottom = new JPanel();
		
		// Initially no sorts are set
		isSortSelected = false;
		
		// List of radiobuttons which only one can be selected at a time
		uniqueSortOpts = new LinkedList<JRadioButton>();
		
		// Initialises list for storing radio buttons
		sortOpts = new LinkedList<JRadioButton>();		
		
		// Sets sizes for margins
		top.setPreferredSize(new Dimension(this.getPreferredSize().width, 40));
		lMargin.setPreferredSize(new Dimension(10, this.getPreferredSize().height));
		rMargin.setPreferredSize(new Dimension(10, this.getPreferredSize().height));
		bottom.setPreferredSize(new Dimension(this.getPreferredSize().width, 10));
		
		// Sets the main
		setLayout(new BorderLayout());
		
		// Configures main body panel, which 5 sub panels are added
		body.setBackground(new Color(230, 230, 230));
		body.setLayout(new GridLayout(5, 1));
		
		// Adds redundant panels used to artificially create margins
		add(top, BorderLayout.NORTH);
		add(body, BorderLayout.CENTER);
		add(lMargin, BorderLayout.WEST);
		add(rMargin, BorderLayout.EAST);
		add(bottom, BorderLayout.SOUTH);

		//Panel 1: First panel used to select the directory
		// Main contnainer panel is dirSelect
		JLabel dirSelectTitle = new JLabel("Please select your directory");
		JPanel dirSelectOption = new JPanel(new GridLayout(2, 2));
		JPanel dirSelect = new JPanel(new GridLayout(2, 1));
		dirSelect.add(dirSelectTitle);
		dirSelect.add(dirSelectOption);
		dirSelect.setBorder(BorderFactory.createEtchedBorder());
		
		// Initialises and panels variables and adds action listener
		dirText = new JTextField();
		browse = new JButton("Browse");
		browse.addActionListener(this);
	        
		// adds directory select label and browse button
		// as well as positions components accordingly
        dirSelectOption.add(dirText);
		JPanel brwsButton = new JPanel(new GridLayout(1, 3));
		brwsButton.add(new JPanel());
		brwsButton.add(new JPanel());
		brwsButton.add(browse);
		dirSelectOption.add(brwsButton);
		dirSelectOption.add(new JPanel());
		dirSelectOption.add(new JPanel());
		body.add(dirSelect);
		
		//Panel 3: Panel concerning the various organisation options
		// The main panel is sortOptionsBody which consists of
		// several other sub panels
		JPanel sortOptionsBody = new JPanel(new BorderLayout());
		JPanel sortOptions = new JPanel(new GridLayout(3, 1));
		
		// Creates radio buttons for each option available, and adds
		// an actionlistener
		sortOpts.add(new JRadioButton("Date Created"));
		sortOpts.add(new JRadioButton("Date Modified"));
		sortOpts.add(new JRadioButton("Month Created"));
		sortOpts.add(new JRadioButton("Date Accessed"));
		sortOpts.add(new JRadioButton("File Name (First character)"));
		sortOpts.add(new JRadioButton("Extension (.docx)"));
		for (JRadioButton temp : sortOpts) {
			temp.addActionListener(this);
			sortOptions.add(temp);
			uniqueSortOpts.add(temp);
		}
		
		// Sets border and add sort options containing all the possible
		// organisation options
		sortOptionsBody.setBorder(BorderFactory.createTitledBorder("Please select your sort options"));
		sortOptionsBody.add(sortOptions, BorderLayout.CENTER);
		
		// Creates new panel for additional features
		// and stores all the panels in the main panel 
		JPanel sortOptionsExtra = new JPanel(new BorderLayout());
		advFeatures = new JButton("Advanced");
		sortOptionsExtra.add(advFeatures, BorderLayout.EAST);
		sortOptionsBody.add(sortOptionsExtra, BorderLayout.SOUTH);
		body.add(sortOptionsBody);
		
		// Panel setup for the file types which will be organised
		JPanel fileTypeSelect = new JPanel(new GridLayout(2, 1));
		fileTypeSelect.setBorder(BorderFactory.createTitledBorder("Please select the file"
				+ " types which will be effected"));
		fileTypeSelect.add(new JLabel("Please seperate with a single space ' ' in format i.e. pdf jpg..."));
		
		JPanel fileTypeOptions = new JPanel(new GridLayout(1, 2));
		fileTypesText = new JTextField();
		fileTypesText.addKeyListener(this);
		fileTypesText.setEnabled(false);
		allFiles = new JRadioButton("All File Types");
		
		// Initially all files are selected
		allFiles.setSelected(true);
		allFiles.addActionListener(this);
		
		// adds components to main panel
		fileTypeOptions.add(fileTypesText);
		fileTypeOptions.add(allFiles);
		fileTypeSelect.add(fileTypeOptions);
		body.add(fileTypeSelect);
	
		//Panel 4: This panel will remain blank for the time being
		JPanel panel4Main = new JPanel(new BorderLayout());

		// Textfield to enter text for adv. options
		substrField = new JTextField();
		
		
		advSortOpts = new LinkedList<JRadioButton>();
		advSortOpts.add(new JRadioButton("By Word"));
		advSortOpts.add(new JRadioButton("Ignore Prefix (aplhab.. sort)"));
		
		// Sets layout for panel and adds the textfield
		panel4Main.setLayout(new GridLayout(3, 3));
		panel4Main.add(substrField);
		panel4Main.add(Box.createRigidArea(new Dimension(1, 0)));
		panel4Main.add(Box.createRigidArea(new Dimension(1, 0)));
		
		// Add adv sort option buttons and fill the remaining space
		// with rigid boxes
		for (int i = 0; i < 6; i++) {
			if (i < advSortOpts.size()) {
				// add each advanced sort option button
				JRadioButton jRB = advSortOpts.get(i);
				panel4Main.add(jRB);
				uniqueSortOpts.add(jRB);
				jRB.addActionListener(this);			
			} else {
				// Rigid areas which can be used later for expansion
				panel4Main.add(Box.createRigidArea(new Dimension(1, 0)));
			}
		}

		body.add(panel4Main);
		
		//Panel 5: Contains a done button to begin the process
		// and a help button providing additional information
		// regarding the use of this application
		// Numerous JPanels below are used for style and positioning
		// purposes
		// Main panel for this sub panel is finishPanel
		JPanel finishPanel = new JPanel(new GridLayout(1, 5));
		JPanel moreInfo = new JPanel(new BorderLayout());
		JPanel moreInfoButton = new JPanel(new GridLayout(1, 3));
		JPanel center = new JPanel(new GridLayout(3, 1));
		helpButton = new JButton("Help");
		done = new JButton("Done");
		done.addActionListener(this);
		
		// adds all components to its correct panel
		center.add(done);
		finishPanel.add(new JPanel());
		finishPanel.add(new JPanel());
		finishPanel.add(center);
		finishPanel.add(new JPanel());
		finishPanel.add(moreInfo);
		moreInfoButton.add(helpButton);
		moreInfoButton.add(new JPanel());
		moreInfo.add(moreInfoButton, BorderLayout.SOUTH);
		body.add(finishPanel);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getActionCommand().equals("Browse")) {
			// General file chooser setup, including restricting options
	    	// to directories only
	    	JFileChooser chooser = new JFileChooser(); 
	        chooser.setCurrentDirectory(new java.io.File("."));
	        chooser.setDialogTitle("Select Directory");
	        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        
	        // Prevent Select of other files
	        chooser.setAcceptAllFileFilterUsed(false);
	        
	        // Handles option selected
	        if (chooser.showOpenDialog(main) == JFileChooser.APPROVE_OPTION) { 
	          dirText.setText(chooser.getSelectedFile().getAbsolutePath());
	        }
		} else if (arg0.getSource() instanceof JRadioButton) {
			if (arg0.getSource() == allFiles) {
				JRadioButton jb = (JRadioButton)arg0.getSource();
				if (jb.isSelected()) {
					fileTypesText.setEnabled(false);;
				} else {
					fileTypesText.setEnabled(true);
				}
			} else {
				// Handles case if button click is from a radio button
				JRadioButton jb = (JRadioButton)arg0.getSource();
				for (JRadioButton temp : uniqueSortOpts) {
					temp.setSelected(false);
				}
				jb.setSelected(true);
				isSortSelected = true;		
			}
		} else if (arg0.getActionCommand().equals("Done")) {
			organiseData();
		}
	}

	/**
	 * Checks post conditions
	 * if they fail will create an error
	 * else if successful will start the file
	 * organiser
	 */
	public void organiseData () {
		int i = checkPostCond();
		if (i == -1) {
			FileOrganiser fO = new FileOrganiser(dirText.getText());
			setSortStrat(fO);
			if (!allFiles.isSelected()) {
				fO.setFileTypes(fileTypesText.getText());
				fO.setTargetAllFiles(false);
			}
			Thread organiser = new Thread(fO);
			organiser.start();
		} else {
			emptyFieldError(i);	
		}
	}
	
	/**
	 * Post condition tests
	 * returns -1 if all the data is available to 
	 * proceed with the next step
	 * @return -1 if the class is in a stable state
	 */
	public int checkPostCond () {
		if (dirText.getText().length() == 0) return 0;
		if (isSortSelected == false) return 1;
		File fTemp = new File(dirText.getText());
		if (!fTemp.exists()) return 2;
		if (!allFiles.isSelected() && fileTypesText.getText().length() == 0) return 3;
		String advSortText =  substrField.getText();
		for (JRadioButton temp : advSortOpts) {
			if (temp.isSelected() && advSortText.length() == 0) {
				return 4; 
			}
		}
		return -1;
	}
	
	/**
	 * Used to set the sort strategy
	 * depending on users selection
	 * @param fO object to set strategy within
	 */
	public void setSortStrat (FileOrganiser fO) {		
		JRadioButton temp;
		SortStrategy sS = null;
		for (int i = 0; i < sortOpts.size(); i++) {
			temp = sortOpts.get(i);
			if (temp.isSelected()) {
				switch (i) {
				case 0: sS = new StrategyDateCreated(); break;
				case 1: sS = new StrategyDateModified(); break;
				case 2: sS = new StrategyMonthCreated(); break;
				case 3: sS = new StrategyDateAccessed(); break;
				case 4: sS = new StrategyFirstChar(); break;
				case 5: sS = new StrategyExtension(); break;
				}
			}
		}
		fO.addStrategy(sS);
	}
	
	/**
	 * Displays appropriate error given error code
	 * @param errorType error code
	 */
	public void emptyFieldError (int errorType) {
		// Sets the appropriate error, given the error code
		String error;
		switch (errorType) {
		case 0: error = "directory"; break;
		case 1: error = "organisaion method"; break;
		case 2: error = "valid directory"; break;
		case 3: error = "an extension"; break;
		case 4: error = "text in adv. sort textfield"; break;
		default: error = "unknown";
		}
		
		// Displays alert with appropriate error text
		JOptionPane.showMessageDialog(this,
			    "Please select a " + error + " above.",
			    "Missing Field",
			    JOptionPane.WARNING_MESSAGE);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	/**
	 * KeyReleased function used to decactivate the all files
	 * selected option in the gui
	 * @param arg0
	 */
	@Override
	public void keyReleased(KeyEvent arg0) {
		if (arg0.getSource() instanceof JTextField) {
			JTextField temp = (JTextField)arg0.getSource();
			if (temp == fileTypesText) {
				temp.setEnabled(true);
				allFiles.setSelected(false);
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
